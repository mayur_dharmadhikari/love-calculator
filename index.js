'use strict';
const Alexa = require('alexa-sdk');

//Replace with your app ID (OPTIONAL).  You can find this value at the top of your skill's page on http://developer.amazon.com.
//Make sure to enclose your value in quotes, like this:  var APP_ID = "amzn1.ask.skill.b692f5e4-0f6e-44e6-8405-f1efe7c5d03e";
var APP_ID = "amzn1.ask.skill.b692f5e4-0f6e-44e6-8405-f1efe7c5d03e";


var welcom_message_1st = [
    " Hi, ", //0
    " Hello, ", //1
    " Hey there ! ", //2
    " Hello, " //3
];
var welcom_message_2nd = [
    " Welcome to Love Calculator. Let's find out your love score. ",
    " Welcome to Love Calculator. It's a pleasure to meet you. Let's find out your love score. "
];

var get_name = [
    " Tell me your name ",
    " Whats your name ",
    " Can I get your name ",
    " Your name please ",
    " Your good name? ",
    " May I know your name please ",
    " May I ask your name "
];
var name_describer = [
    " Wow,nice name. ",
    " Good name. ",
    " Cool, nice name. ",
    " Its a nice name. ",
    " You’ve got a nice name. ",
    " Cool, I like this name. ",
    " Thats a beautiful name. "
];
var get_second_name = [
    " whom do you love most? tell me the name ",
    " your lovers name please ",
    " tell me your crush or lover’s name ",
    " may I know your crush’s name ",
    " Please tell me your love’s name "
];
var exit_message = [
    " Have a great life see you agin, bye ",
    " bye, see you soon ",
    " byeee,  take care ",
    " byee, have a nice day ",
    " bye, c ya ",
    " ba bye "
];

//This is the message a user will hear when they ask Alexa for help in your skill.
var HELP_MESSAGE = "You can check Love Percentage using Love Calculator. Say Launch Love Calculator to launch the skill.";

var firstName = "";
var secondName = "";
var finalScore = 60;


exports.handler = (event, context) => {
    const alexa = Alexa.handler(event, context);
    alexa.appId = APP_ID;
    alexa.registerHandlers(launchHandler, basicHandler);
    alexa.execute();
};

var launchHandler = {
    'LaunchRequest': function() {
        var date = new Date();
        var current_hour = date.getHours();
        if (current_hour > 0 && current_hour < 12) {
            this.emit(":ask", welcom_message_1st[getRandom(0, 3)] + " Good Morning! " + welcom_message_2nd[getRandom(0, 1)] + get_name[getRandom(0, 6)]);
        } else if (current_hour > 12 && current_hour < 6) {
            this.emit(":ask", welcom_message_1st[getRandom(0, 3)] + " Good Afternoon! " + welcom_message_2nd[getRandom(0, 1)] + get_name[getRandom(0, 6)]);
        } else {
            this.emit(":ask", welcom_message_1st[getRandom(0, 3)] + " Good Evening! " + welcom_message_2nd[getRandom(0, 1)] + get_name[getRandom(0, 6)]);
        }
    },
    'EndGameIntent': function() {
        this.emit(':tell', exit_message[getRandom(0, 5)]);
    },
    'AMAZON.HelpIntent': function() {
        this.emit(':tell', HELP_MESSAGE);
    },
    'AMAZON.StopIntent': function() {
        this.emit(':tell', exit_message[getRandom(0, 5)]);
    },
    'AMAZON.CancelIntent': function() {
        this.emit(':tell', exit_message[getRandom(0, 5)]);
    },
    'Unhandled': function() {
        this.emit(':ask', 'Sorry, I didn\'t get that. Try saying a again.');
    }
};

var basicHandler = {
    'NameIntent': function() {
        var anyName = "";

        console.log("In FirstNameIntent." +
            " First Name : " + this.event.request.intent.slots.firstname.value +
            " Middle Name : " + this.event.request.intent.slots.middlename.value +
            " Last Name : " + this.event.request.intent.slots.lastname.value);


        if (this.event.request.intent.slots.firstname.value !== undefined) {
            anyName += " ";
            anyName += this.event.request.intent.slots.firstname.value;
        }
        if (this.event.request.intent.slots.middlename.value !== undefined) {
            anyName += " ";
            anyName += this.event.request.intent.slots.middlename.value;
        }
        if (this.event.request.intent.slots.lastname.value !== undefined) {
            anyName += " ";
            anyName += this.event.request.intent.slots.lastname.value;
        }

        console.log("Printing anyName : " + anyName);
        var generatedName = anyName.trim();
        console.log("Printing Generated Name : " + generatedName);


        if (generatedName.toLowerCase() === "check".toLowerCase()) {
            console.log("In CheckCase " + finalScore + firstName + secondName);
            if (finalScore > 60 && finalScore < 70) {
                this.emit(":ask", " As your score is " + finalScore + ". The chance of a relationship working out between " + firstName + " and " + secondName + " is not very big, but a relationship is very well possible, if the two of you really want it to, and are prepared to make some sacrifices for it. You'll have to spend a lot of quality time together. You guys would rather spend a lifetime with each other than face all the ages of this world alone." +
                    " Do you want to play again ? Say Play more to play again and No to exit.");
            } else if (finalScore > 70 && finalScore < 80) {
                this.emit(":ask", " As your score is " + finalScore + ". Darkness cannot drive out darkness: only light can do that. Hate cannot drive out hate: only love can do that. Both of you are blessed with the light of love!!" +
                    " Do you want to play again ? Say Play more to play again and No to exit.");
            } else if (finalScore > 80 && finalScore < 90) {
                this.emit(":ask", " As your score is " + finalScore + ". For you guys, Love is just a word, it’s a definition you guys give each other. You look at each other and see the rest of your life in each others eyes. A God given blessing!!" +
                    " Do you want to play again ? Say Play more to play again and No to exit.");
            } else if (finalScore > 90 && finalScore < 99) {
                this.emit(":ask", " As your score is " + finalScore + ". Being deeply loved by someone gives you strength, while loving someone deeply gives you courage. You guys are blessed with both!! You guys chose each other. And will choose each other over and over and over. Without pause, without a doubt, in a heartbeat. Lucky guys!!" +
                    " Do you want to play again ? Say Play more to play again and No to exit.");
            }
        } else if (generatedName.toLowerCase() === "Play more".toLowerCase()) {
            console.log("In PlayMoreIntent " + finalScore + firstName + secondName);
            clearAttributes(this);
            this.emit(":ask", "Cool, Let's start again. " + get_name[getRandom(0, 6)]);
        } else if (firstName.length === 0) {
            firstName = generatedName;
            console.log("Got First Name : " + firstName);
            this.emit(":ask", name_describer[getRandom(0, 6)] + get_second_name[getRandom(0, 4)]);
        } else if (secondName.length === 0) {
            console.log("Got secondName : " + secondName);
            secondName = generatedName;
            finalScore = calculatePercentage(firstName, secondName);
            this.emit(":ask", name_describer[getRandom(0, 6)] +
                "finding your love score.... <audio src='https://s3.amazonaws.com/ask-soundlibrary/magic/amzn_sfx_fairy_melodic_chimes_01.mp3'/>" +
                " and your love score is : " + finalScore + ". Do you want to know what this score means? Say check to check or No to exit.");
        } else if (generatedName.toLowerCase() === "No".toLowerCase()) {
            clearAttributes(this);
            this.emit(':tell', exit_message[getRandom(0, 5)]);
        } else {
            clearAttributes(this);
            this.emit(":tell", "Sorry, look like something is wrong. Please try again to start");
        }
    },
    'EndSkillIntent': function() {
        clearAttributes(this);
        this.emit(':tell', exit_message[getRandom(0, 5)]);
    },
    'AMAZON.HelpIntent': function() {
        this.emit(':tell', HELP_MESSAGE);
    },
    'AMAZON.StopIntent': function() {
        clearAttributes(this);
        this.emit(':tell', exit_message[getRandom(0, 5)]);
    },
    'AMAZON.CancelIntent': function() {
        clearAttributes(this);
        this.emit(':tell', exit_message[getRandom(0, 5)]);
    },
    'Unhandled': function() {
        clearAttributes(this);
        this.emit(':ask', 'Sorry, I didn\'t get that. Try saying a again.');
    }
};

function calculatePercentage(firstName, secondName) {
    console.log("In calculatePercentage : firstname : " + firstName + " secondName : " + secondName);
    var score = 0;
    var fullName = firstName.toLowerCase() + secondName.toLowerCase();
    var trueLove = "truelove".split('');
    var arrayName = fullName.split('');
    for (var i = 0; i < trueLove.length; i++) {
        var ch = trueLove[i];
        for (var j = 0; j < arrayName.length; j++) {
            var name = arrayName[j];
            if (name == ch) {
                score++;
                console.log("Score : " + score);
            }
        }
    }

    //console.log("Score : " + score);
    var finalScore = (score % 40) + 60;
    console.log("Final Score of : " + firstName + " and " + secondName + " is : " + finalScore);
    return finalScore;
}

function clearAttributes(thisValue) {
    console.log("In clearAttributes");
    firstName = "";
    secondName = "";
    finalScore = 60;
}

function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}